use crate::Dimensions;
use mcai_types::Coordinates;

#[derive(Clone, Debug)]
pub struct GraphConfiguration {
  node: NodeConfiguration,
}

impl GraphConfiguration {
  pub fn new(node: NodeConfiguration) -> Self {
    Self { node }
  }

  pub fn node_configuration(&self) -> &NodeConfiguration {
    &self.node
  }
}

#[derive(Clone, Debug, PartialEq)]
pub struct NodeConfiguration {
  width: usize,
  height: usize,
  x_gap: usize,
  y_gap: usize,
}

impl NodeConfiguration {
  pub fn new(width: usize, height: usize, x_gap: usize, y_gap: usize) -> Self {
    Self {
      width,
      height,
      x_gap,
      y_gap,
    }
  }

  pub fn width(&self) -> usize {
    self.width
  }

  pub fn height(&self) -> usize {
    self.height
  }

  pub fn x_gap(&self) -> usize {
    self.x_gap
  }

  pub fn y_gap(&self) -> usize {
    self.y_gap
  }

  pub fn get_dimensions(&self) -> Dimensions {
    Dimensions::new(self.width, self.height)
  }

  pub fn get_coordinates(&self, row: isize, column: isize) -> Coordinates {
    let x = column * (self.width + self.x_gap) as isize;
    let y = row * (self.height + self.y_gap) as isize;

    Coordinates::new(x, y)
  }
}
